# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Environment setup for ROCm 5.4.6.
#

export PATH=/opt/rocm-5.4.6/bin${PATH:+:${PATH}}
