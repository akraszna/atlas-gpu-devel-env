# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building a Docker image that allows ATLAS developers
# to conveniently develop offline code for GPUs with.
#

# Base the image on vanilla AlmaLinux 9.
FROM almalinux:9

# Perform the installation as root.
USER root
WORKDIR /root

# Install a basic GCC 11 development environment. The directories/files under
# /opt/lcg are only needed to fool asetup into thinking that it's setting up
# GCC from an LCG installation.
RUN echo "max_parallel_downloads=10" >> /etc/dnf/dnf.conf &&                   \
    dnf install -y which gcc gcc-c++ gcc-gfortran sudo libaio-devel man-db     \
                   libSM atlas-devel libX11-devel libXpm-devel libuuid-devel   \
                   libXft-devel libXext-devel libXi-devel git expat-devel      \
                   mesa-libGL-devel mesa-libGLU-devel mesa-libEGL-devel        \
                   gmp-devel libcurl-devel glib2-devel xz-devel rpm-build wget \
                   dnf-plugins-core python3 epel-release openssl-devel         \
                   procps-ng &&                                                \
    dnf --enablerepo=crb install -y libtirpc-devel &&                          \
    dnf clean all &&                                                           \
    mkdir -p /opt/lcg/gcc/x86_64-el9 &&                                        \
    touch /opt/lcg/gcc/x86_64-el9/setup.sh

# Install CUDA 12.3.
ARG CUDA_VERSION=12-3
RUN dnf config-manager --add-repo                                              \
    https://developer.download.nvidia.com/compute/cuda/repos/rhel9/x86_64/cuda-rhel9.repo && \
    dnf install -y cuda-compat-${CUDA_VERSION} cuda-cudart-${CUDA_VERSION}     \
                   cuda-libraries-${CUDA_VERSION}                              \
                   cuda-command-line-tools-${CUDA_VERSION}                     \
                   cuda-minimal-build-${CUDA_VERSION} &&                       \
    dnf clean all
COPY docker/cuda-${CUDA_VERSION}.sh /etc/profile.d/
ENV NVIDIA_VISIBLE_DEVICES=all
ENV NVIDIA_DRIVER_CAPABILITIES=compute,utility

# Install ROCm 5.4.6.
ARG ROCM_VERSION=5.4.6
COPY docker/rocm-${ROCM_VERSION}.repo /etc/yum.repos.d/
RUN dnf --enablerepo=crb install -y                                            \
        rocminfo${ROCM_VERSION} rocm-smi${ROCM_VERSION}                        \
        rocm-device-libs${ROCM_VERSION} rocm-cmake${ROCM_VERSION}              \
        rocm-gdb${ROCM_VERSION} rocm-utils${ROCM_VERSION}                      \
        hip-base${ROCM_VERSION} hip-rocclr${ROCM_VERSION}                      \
        llvm-amdgpu${ROCM_VERSION} &&                                          \
    dnf clean all
COPY docker/rocm-${ROCM_VERSION}.sh /etc/profile.d/
COPY docker/rocm-${ROCM_VERSION}.conf /etc/ld.so.conf.d/

# Install oneAPI 2023.2.1.
COPY docker/oneapi.repo /etc/yum.repos.d/
COPY packages/oneapi-*.sh /root/
RUN dnf install -y intel-oneapi-compiler-dpcpp-cpp-2023.2.1                    \
                   intel-oneapi-tbb-devel-2021.10.0 &&                         \
    dnf clean all &&                                                           \
    /root/oneapi-for-nvidia-gpus-2023.2.1-cuda-12.0-linux.sh --yes             \
        --install-dir /opt/intel/oneapi &&                                     \
    /root/oneapi-for-amd-gpus-2023.2.1-rocm-5.4.3-linux.sh --yes               \
        --install-dir /opt/intel/oneapi &&                                     \
    rm /root/oneapi*.sh
COPY docker/oneapi.sh /etc/profile.d/

# Install CMake 3.27.5 and Ninja 1.11.1.
ARG CMAKE_VERSION=3.27.5
ARG NINJA_VERSION=1.11.1
RUN wget https://cmake.org/files/v3.27/cmake-${CMAKE_VERSION}-linux-x86_64.tar.gz && \
    mkdir -p /opt/cmake/${CMAKE_VERSION}/Linux-x86_64 &&                       \
    tar -C /opt/cmake/${CMAKE_VERSION}/Linux-x86_64 --strip-components=1       \
        --no-same-owner -xvf cmake-${CMAKE_VERSION}-linux-x86_64.tar.gz &&     \
    rm cmake-${CMAKE_VERSION}-linux-x86_64.tar.gz &&                           \
    wget https://github.com/ninja-build/ninja/releases/download/v${NINJA_VERSION}/ninja-linux.zip && \
    mkdir -p /opt/ninja/${NINJA_VERSION}/Linux-x86_64 &&                       \
    unzip ninja-linux.zip -d /opt/ninja/${NINJA_VERSION}/Linux-x86_64 &&       \
    chmod 755 /opt/ninja/${NINJA_VERSION}/Linux-x86_64/ninja &&                \
    rm ninja-linux.zip
COPY docker/cmake-${CMAKE_VERSION}.sh docker/ninja-${NINJA_VERSION}.sh \
     /etc/profile.d/

# Set up the ATLAS user, and give it "all the rights it needs". Note the the
# group IDs are hard-coded to the values that I have on my Ubuntu 20.04 host.
# Unfortunately I just didn't find a way to avoid this, while still not using
# the root user on both the host and in the container to use my AMD GPU...
RUN ldconfig &&                                                                \
    echo '%wheel	ALL=(ALL)	NOPASSWD: ALL' >> /etc/sudoers &&              \
    groupmod -g 44 video && groupmod -g 110 render &&                          \
    adduser atlas && chmod 755 /home/atlas &&                                  \
    usermod -aG wheel,video,render atlas &&                                    \
    mkdir /workdir && chown "atlas:atlas" /workdir &&                          \
    chmod 755 /workdir

# Set up the ATLAS specific runtime environment/configuration.
COPY docker/atlas_prompt.sh docker/atlas_env.sh /etc/profile.d/
COPY docker/.asetup /home/atlas/

# Add basic usage instructions for the image.
COPY docker/motd /etc/

# Switch to the ATLAS user.
USER atlas
WORKDIR /workdir

# Start the image with BASH by default, after having printed the message
# of the day.
CMD cat /etc/motd && /bin/bash
